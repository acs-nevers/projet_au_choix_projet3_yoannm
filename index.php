<?php
$title = 'Derniers articles';
include 'include/header.php';
?>

<main id="last-articles">
  <h2>Derniers articles</h2>

  <?php
  lastArticles();
  ?>
</main>

<?php
include 'include/footer.php';
?>
