<?php
$title = 'S\'identifier';
include 'include/header.php';
?>

<main id="login">
  <nav>
    <div class="tab" id="choose-sign-in">S'identifier</div>
    <div class="tab inactive" id="choose-sign-up">S'enregistrer</div>
  </nav>

  <form id="sign-in" method="post" action="login.php">
    <input type="text" name="username" placeholder="Identifiant">
    <input type="password" name="password" placeholder="Mot de passe">
    <input type="submit" name="sign-in">
  </form>

  <form id="sign-up" class="hidden" method="post" action="/blog/php/processing.php">
    <input type="text" name="username" placeholder="Identifiant">
    <input type="password" name="password" placeholder="Mot de passe">
    <input type="password" name="confirm-password" placeholder="Confirmer mot de passe">
    <input type="text" name="display-name" placeholder="Nom d'auteur">
    <input type="submit" name="sign-up">
  </form>

  <p>
    <?php
      login();
    ?>
  </p>
</main>

<script type="text/javascript" src="/blog/public/js/login.js"></script>

<?php
include 'include/footer.php';
?>