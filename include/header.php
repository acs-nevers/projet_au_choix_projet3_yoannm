<?php
  session_start();

  define('__ROOT__', dirname(dirname(__FILE__)));
  require_once(__ROOT__.'/php/functions.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" href="/blog/public/img/favicon.png">
  <link rel="stylesheet" type="text/css" href="/blog/public/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="/blog/public/css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
  <title>
    <?php
      echo $title;
    ?>
  </title>
</head>

<body>

  <header id="main-header">
    <h1><a href="/blog">Blog</a></h1>
    <?php
      isLoggedIn();
    ?>
  </header>
