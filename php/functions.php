<?php

function isLoggedIn() {
  if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true){
    include(__ROOT__.'/include/loggedout-menu.php');
  } else {
    include(__ROOT__.'/include/loggedin-menu.php');
  }
}

function dbConnect() {
  require 'mysql.php';

  $host = 'localhost';
  $charset = 'utf8';

  $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

  try {
    $pdo = new PDO($dsn, $user, $password);
  } catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
  }

  return $pdo;
}

function test_input($data) {
  $data = trim($data); // removes extra spaces and line endings
  $data = stripslashes($data); // strips antislashes
  $data = htmlspecialchars($data); // escapes special characters
  return $data;
}

function createUser() {
  $username = $password = $display_name = '';
  $errors = 0;
  $errorMsg = '';

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['sign-up'])) {

    if (empty($_POST['username'])) {
      $errorMsg .= 'Vous devez renseigner un identifiant. <br>';
      $errors++;
    } elseif (strlen($_POST['username']) < 6){
      $errorMsg .= 'Votre identifiant doit faire plus de 6 caractères. <br>';
      $errors++;
    } else {
      $username = test_input($_POST['username']);
    }

    if (empty($_POST['password'])) {
      $errorMsg .= 'Vous devez renseigner un mot de passe. <br>';
      $errors++;
    } elseif (strlen($_POST['password']) < 7){
      $errorMsg .= 'Votre mot de passe doit faire plus de 7 caractères. <br>';
      $errors++;
    } elseif ($_POST['password'] != $_POST['confirm-password']) {
      $errorMsg .= 'Le mot de passe et la confirmation doivent
        être identiques <br>';
      $errors++;
    } else {
      $password = crypt($_POST['password'], 'XH=jd4s68/x');
    }

    if (empty($_POST['display-name'])) {
      $errorMsg .= 'Vous devez renseigner un nom d\'auteur. <br>';
      $errors++;
    } elseif (strlen($_POST['display-name']) < 2){
      $errorMsg .= 'Votre nom d\'auteur doit faire plus de 2 caractères. <br>';
      $errors++;
    } else {
      $display_name = test_input($_POST['display-name']);
    }
  }

  $pdo = dbConnect();

  $sql = 'INSERT INTO authors (username, password, display_name)
    VALUES (:username, :password, :display_name)';

  if ($errors == 0) {
    // update database with new user
    $query = $pdo->prepare($sql);
    $query->execute(['username' => $username, 'password' => $password,
      'display_name' => $display_name]);
    // log in
    $_SESSION['loggedin'] = true;
    $_SESSION['user'] = $display_name;
    // redirect to home page
    header('Location: /blog');
  } else {
    echo $errorMsg;
  }
}

function login() {
  $pdo = dbConnect();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['sign-in'])) {
    $username = test_input($_POST['username']);
    $password = crypt($_POST['password'], 'XH=jd4s68/x');

    $sql = "SELECT * FROM authors WHERE username = :username";

    $query = $pdo->prepare($sql);
    $query->execute(['username' => $username]);

    if ($query->rowCount() === 0) {
      echo 'Cet utilisateur n\'existe pas';
    } else {
      $result = $query->fetch();
      $storedPassword = $result['password'];
      $display_name = $result['display_name'];

      if ($storedPassword == $password) {
        $_SESSION['loggedin'] = true;
        $_SESSION['user'] = $display_name;

        header('Location: /blog');
      } else {
        echo 'Mauvais mot de passe';
      }
    }
  }
}

function logout() {
  // remove all session variables
  session_unset();

  // destroy the session
  session_destroy();

  header('Location: .');
}

function writeArticle() {
  $pdo = dbConnect();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['write-article'])) {

    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
      echo '<p>Connectez-vous pour pouvoir écrire un article</p>';
    } else {

      $errorMsg = '';
      $errors = 0;

      // title
      if (empty($_POST['title'])) {
        $errorMsg .= 'Vous devez renseigner un titre. <br>';
        $errors++;
      } elseif (strlen($_POST['title']) < 10){
        $errorMsg .= 'Votre titre doit faire plus de 10 caractères. <br>';
        $errors++;
      } else {
        $title = test_input($_POST['title']);
      }

      // article content
      if (empty($_POST['text'])) {
        $errorMsg .= 'Votre article est vide. <br>';
        $errors++;
      } elseif (strlen($_POST['text']) < 50){
        $errorMsg .= 'Votre article doit faire plus de 50 caractères. <br>';
        $errors++;
      } else {
        $article = test_input($_POST['text']);
      }

      if ($errors === 0) {

        // Get author id
        $user = $_SESSION['user'];
        $sql = 'SELECT id_author FROM authors WHERE display_name = :user';

        $query = $pdo->prepare($sql);
        $query->execute(['user' => $user]);

        $author_id = $query->fetch()['id_author'];

        // Insert article info into DB
        $sql = 'INSERT INTO articles VALUES (NULL, :title, :article, NOW(), "", :author_id)';

        $query = $pdo->prepare($sql);
        $query->execute(['title' => $title, 'article' => $article,
          'author_id' => $author_id]);

        // Get last article id
        $sql = 'SELECT LAST_INSERT_ID() FROM articles';
        $query = $pdo->query($sql);
        $article_id = $query->fetch()[0];

        // categories
        if (!empty($_POST['category'])) {
          foreach($_POST['category'] as $selected) {
            $sql = 'SELECT * FROM categories WHERE category_name = :selected';
            $queryS = $pdo->prepare($sql);
            $queryS->execute(['selected' => $selected]);

            if ($queryS->rowCount() === 0) {
              $sql = 'INSERT INTO categories VALUES (NULL, :selected)';
              $query = $pdo->prepare($sql);
              $query->execute(['selected' => $selected]);

              $sql = 'SELECT LAST_INSERT_ID() FROM categories';
              $query = $pdo->query($sql);
              $category_id = $query->fetch()[0];
            } else {
              $category_id = $queryS->fetch()['id_category'];
            }

            $sql = 'INSERT INTO have_category VALUES (:category_id, :article_id)';
            $query = $pdo->prepare($sql);
            $query->execute(['category_id' => $category_id,
              'article_id' => $article_id]);
          }
        }

        header('Location: /blog/article.php?id='.$article_id);
      } else {
        echo $errorMsg;
      }
    }
  }
}

function lastArticles() {
  $pdo = dbConnect();

  $sql = 'SELECT * FROM articles JOIN
  authors ON articles.id_author = authors.id_author
  ORDER BY id_article DESC LIMIT 10';

  $query = $pdo->query($sql);

  while ($row = $query->fetch()) {

    $text = $row['article_text'];
    $text = substr($text, 0, 150);

    $date = $row['date'];
    $date = strtotime($date);
    $date = date( 'd/m/Y à H:i', $date );

    // Display article
    echo '<article>';
    echo '<header>';
    echo '<h3 class=title><a href="/blog/article.php?id='.$row['id_article'].'">'.
      $row['title'].'</a></h3>';
    echo '<div class="info">';
    echo '<div>le '.$date.'</div>';
    echo '<div>par '.$row['display_name'].'</div>';
    echo '</div>';
    echo '</header>';
    echo '<p>'.$text.'...</p>';
    echo '<a class="read" href="/blog/article.php?id='.$row['id_article'].
      '">Lire la suite</a>';
    echo '</article>';
  }
}

function article() {
  $article_id = $_GET['id'];

  $pdo = dbConnect();
  $sql = 'SELECT * FROM articles JOIN
    authors ON articles.id_author = authors.id_author
    WHERE articles.id_article = :article_id';

  $query = $pdo->prepare($sql);
  $query->execute(['article_id' => $article_id]);

  $result = $query->fetch();

  $title = $result['title'];
  $date = $result['date'];
  $date = strtotime($date);
  $date = date( 'd/m/Y à H:i', $date );
  $author = $result['display_name'];
  $text = $result['article_text'];
  $id =  $result['id_article'];

  $paragraphs = explode("\n", $text);

  $text = '';
  foreach ($paragraphs as $paragraph) {
    $text .= '<p>'.$paragraph.'</p>';
  }

  $sql = 'SELECT * FROM articles JOIN
    have_category ON articles.id_article = have_category.id_article JOIN
    categories ON have_category.id_category = categories.id_category
    WHERE articles.id_article = :article_id';

  $query = $pdo->prepare($sql);
  $query->execute(['article_id' => $article_id]);

  $categories = [];
  while ($row = $query->fetch()) {
    array_push($categories, $row['category_name']);
  }

  if (empty($categories)) {
    $categories = 'Aucune';
  } else {
    $categories = implode(', ', $categories);
  }

  echo '<section id="article">';
  echo '<header>';

  if(isset($_SESSION['loggedin'])) {
    if ($_SESSION['loggedin'] == true && $author === $_SESSION['user']) {
      echo '<a class="edit" href="/blog/edit.php?id='.$id.'"><i class="fas fa-edit"></i></a>';
    }
  }

  echo '<h2 class=title>'.$title.'</h2>';
  echo '<div class="info">';
  echo '<div>le '.$date.'</div>';
  echo '<div>par '.$author.'</div>';
  echo '</div>';
  echo '<div class="categories">Catégories : '.$categories.'</div>';
  echo '</header>';
  echo '<article>'.$text.'</article>';
  echo '</section>';
}

function edit() {
  $article_id = $_GET['id'];

  $pdo = dbConnect();

  $categories = [
    ['id' => 'cb-game', 'value' => 'Jeu'],
    ['id' => 'cb-prog', 'value' => 'Programmation'],
    ['id' => 'cb-web', 'value' => 'Web'],
    ['id' => 'cb-irl', 'value' => 'IRL']
  ];

  echo '<div class="flex wrap checkboxes">';

  $sql = 'SELECT * FROM have_category JOIN
    categories ON have_category.id_category = categories.id_category
    WHERE id_article = :article_id and
    category_name = :category_name';

  foreach ($categories as $category) {
    $query = $pdo->prepare($sql);
    $query->execute(['article_id' => $article_id, 'category_name' => $category['value']]);

    if ($query->rowCount() === 0) {
      $checked = '';
    } else {
      $checked = 'checked';
    }

    echo '<div class=checkbox>';
    echo '  <input type="checkbox" name="category[]" id="'.
      $category['id'].'"value="'.$category['value'].'"'.$checked.'>';
    echo '  <label for="'.$category['id'].'">'.$category['value'].'</label>';
    echo '</div>';
  }

  echo '</div>';

  $sql = "SELECT * FROM articles WHERE articles.id_article = :article_id";

  $query = $pdo->prepare($sql);
  $query->execute(['article_id' => $article_id]);

  $result = $query->fetch();

  echo '<h4>Titre :</h4>';
  echo '<input type="text" name="title" placeholder="Titre de l\'article"
    value="'.$result['title'].'">';
  echo '<h4>Article :</h4>';
  echo '<textarea name="text" placeholder="Votre article ici...">'.
    $result['article_text'].'</textarea>';
}

function getId() {
  $id = $_GET['id'];
  echo $id;
}

function editArticle() {
  $pdo = dbConnect();

  $article_id = $_GET['id'];

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['write-article'])) {

    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
      echo '<p>Connectez-vous pour pouvoir éditer un article</p>';
    } else {

      $errorMsg = '';
      $errors = 0;

      // title
      if (empty($_POST['title'])) {
        $errorMsg .= 'Vous devez renseigner un titre. <br>';
        $errors++;
      } elseif (strlen($_POST['title']) < 10){
        $errorMsg .= 'Votre titre doit faire plus de 10 caractères. <br>';
        $errors++;
      } else {
        $title = test_input($_POST['title']);
      }

      // article content
      if (empty($_POST['text'])) {
        $errorMsg .= 'Votre article est vide. <br>';
        $errors++;
      } elseif (strlen($_POST['text']) < 50){
        $errorMsg .= 'Votre article doit faire plus de 50 caractères. <br>';
        $errors++;
      } else {
        $article = test_input($_POST['text']);
      }

      if ($errors === 0) {

        $sql = 'UPDATE articles SET title = :title, article_text = :article
          WHERE id_article = :article_id';

        $update = $pdo->prepare($sql);
        $update->execute(['title' => $title, 'article' => $article,
          'article_id' => $article_id]);

        // categories

        // Delete entries for this article id
        $sql = 'DELETE FROM have_category WHERE id_article = :article_id';

        $delete = $pdo->prepare($sql);
        $delete->execute(['article_id' => $article_id]);

        // Create entries for this article id
        if (!empty($_POST['category'])) {
          foreach($_POST['category'] as $selected) {
            $sql = 'SELECT * FROM categories WHERE category_name = :selected';
            $queryS = $pdo->prepare($sql);
            $queryS->execute(['selected' => $selected]);

            // Create category if it doesn't exist
            if ($queryS->rowCount() === 0) {
              $sql = 'INSERT INTO categories VALUES (NULL, :selected)';
              $query = $pdo->prepare($sql);
              $query->execute(['selected' => $selected]);

              // Get category id
              $sql = 'SELECT LAST_INSERT_ID() FROM categories';
              $query = $pdo->query($sql);
              $category_id = $query->fetch()[0];
            } else {
              $category_id = $queryS->fetch()['id_category'];
            }

            $sql = 'INSERT INTO have_category VALUES
              (:category_id, :article_id)';
            $query = $pdo->prepare($sql);
            $query->execute(['category_id' => $category_id,
              'article_id' => $article_id]);
          }
        }

        header('Location: /blog/article.php?id='.$article_id);

      } else {
        echo $errorMsg;
      }
    }
  }
}

function user() {
  $pdo = dbConnect();

  if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    echo 'Connectez-vous pour accéder à cette page.';

  } else {

    $user = $_SESSION['user'];

    $sql = 'SELECT * FROM articles JOIN
    authors ON articles.id_author = authors.id_author
    WHERE display_name = :user
    ORDER BY id_article DESC';

    $query = $pdo->prepare($sql);
    $query->execute(['user' => $user]);

    while ($row = $query->fetch()) {

      $text = $row['article_text'];
      $text = substr($text, 0, 150);

      $date = $row['date'];
      $date = strtotime($date);
      $date = date( 'd/m/Y à H:i', $date );

      // Display article
      echo '<article>';
      echo '<header>';
      echo '<h3 class=title><a href="/blog/article.php?id='.$row['id_article'].'">'.
        $row['title'].'</a></h3>';
      echo '<div class="info">';
      echo '<div>le '.$date.'</div>';
      echo '<div>par '.$row['display_name'].'</div>';
      echo '</div>';
      echo '</header>';
      echo '<p>'.$text.'...</p>';
      echo '<a class="read" href="/blog/article.php?id='.$row['id_article'].
        '">Lire la suite</a>';
      echo '</article>';
    }
  }
}

function deleteArticle() {

  if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    echo 'Connectez-vous pour effectuer cette opération.';

  } else {

    $pdo = dbConnect();

    $article_id = $_GET['id'];

    $sql = 'DELETE FROM have_category WHERE id_article = :article_id';
    $delete = $pdo->prepare($sql);
    $delete->execute(['article_id' => $article_id]);

    $sql = 'DELETE FROM articles WHERE id_article = :article_id';
    $delete = $pdo->prepare($sql);
    $delete->execute(['article_id' => $article_id]);

    header('Location: /blog');
  }
}

?>
