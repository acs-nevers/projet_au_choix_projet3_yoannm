<?php
$title = 'Ecriture d\'article';
include '../include/header.php';
?>

<main id="post-article">
  <?php
    writeArticle();
  ?>
</main>

<?php
include '../include/footer.php';
?>
