var form = document.getElementById('write-article');
var overlay = document.getElementById('overlay');
var errorsP = document.getElementById('errors');

form.onsubmit = validateForm;

function validateForm() {
  var title = document.getElementsByName('title')[0].value;
  var text = document.getElementsByName('text')[0].value;

  var errors = 0;
  var errorMsg = '';

  if (title.length < 10) {
    errors++;
    errorMsg += 'Votre titre doit faire au moins 10 caractères. <br>';
  }

  if (text.length < 50) {
    errors++;
    errorMsg += 'Votre article doit faire au moins 50 caractères. <br>';
  }

  if (errors === 0) {
    form.submit();
  } else {
    errorsP.innerHTML = errorMsg;
    overlay.classList.add('visible');
    return false;
  }
}

overlay.onclick = function() {
  overlay.classList.remove('visible');
};
