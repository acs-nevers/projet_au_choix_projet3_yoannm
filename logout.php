<?php
$title = 'Déconnexion';
include 'include/header.php';
?>

<main id="login">
  <p>Vous avez été déconnecté</p>
  <?php
    logout();
  ?>
</main>

<?php
include 'include/footer.php';
?>